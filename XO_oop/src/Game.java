import java.util.*;

public class Game {

	Scanner kb = new Scanner(System.in);
	private  Table table;
	private  int row = 0;
	private  int col = 0;
	private  Player o;
	private  Player x;

	Game() {
            x = new Player('X');
            o = new Player('O');
	}

	public  void starGame() {
		table = new Table(x, o);
	}

	public  void showWelcome() {
		System.out.println("Welcome to XO OOP Game");
	}

	public  void showTable() {
		for (int i = 0; i < table.getTable().length; i++) {
			System.out.println("-------------");
			for (int j = 0; j < table.getTable()[i].length; j++) {
				System.out.print("| " + table.getTable()[i][j]);
				System.out.print(" ");
			}
			System.out.print("|");
			System.out.println();
		}
		System.out.println("-------------");
	}
	public  void showTurn() {
		System.out.println("Turn game is : " + table.getCurrentPlayer().getName());
	}
	public  void inputRowCol() {
		System.out.println("Input Row and Col :  ");
		try {
			row = kb.nextInt() - 1;
			col = kb.nextInt() - 1;
			table.setRowCol(row, col);
		} catch (Exception e) {
			showInputMismatch();
			kb.nextLine();
			this.inputRowCol();
		}
	}
	public  void showInputMismatch() {
		System.out.println("Input Mismatch !! Please input number[1-3]");
	}

	public void showDraw() {
		System.out.println("Player x and o is Draw");
	}

	public void showWin() {
		System.out.println("Player " + table.getWinPlayer().getName() + " is Win");
	}

	public  void showGoodbye() {
		System.out.println("Good bye....");
	}

	public  void showInputMismatch1() {
		System.out.println("Input Mismatch !! Please input number[1-3]");
	}

	public  void showContinue() {
		System.out.println("Game is over. Do you want to play again?");
		System.out.println("Please input Y/N : ");
	}
	
	public boolean inputContinue() {
		System.out.print("Do you want to continue playing? (y/n) : ");
		char result = kb.next().charAt(0);
		do {
			if (result != 'y' && result != 'n') {
				System.out.print("Do you want to continue playing? (y/n) : ");
				result = kb.next().charAt(0);
			} else if (result == 'y') {
				return true;
			}
			return false;
		} while (true);
	}
	public void runGame() {
		this.starGame();
		this.showWelcome();
		do {
			this.showTable();
			this.showTurn();
			this.run();
			this.starGame();
		} while (this.inputContinue());
		this.showGoodbye();
	}

	public void run() {
		while (true) {
			this.inputRowCol();
			this.showTable();
			if (table.checkWin()) {
				this.showWin();
				break;
			} else if (table.checkDraw()) {
				this.showDraw();
				break;
			}
			table.swicthTurn();
			this.showTurn();
		}
	}
}
