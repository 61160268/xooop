import java.util.*;
public class Table {
    char[][] table = {{'-','-','-'},{'-','-','-'},{'-','-','-'}};
    Player currentPlayer;
    Player o;
    Player x;
    Player win;
    
    public Table(Player x1, Player o1) {
		x = x1;
		o = o1;
		Random rand = new Random();
		if (rand.nextInt(2) + 1 == 1) {
			currentPlayer = o;
		} else {
			currentPlayer = x;
		}
	}
       
	public char[][] getTable() {
		return table;
	}

	public Player getCurrentPlayer() {
		return currentPlayer;
	}

	public void swicthTurn() {
		if (currentPlayer == x) {
			currentPlayer = o;
		} else {
			currentPlayer = x;
		}
	}

	public Player getWinPlayer() {
		return win;
	}
        public void setRowCol(int row, int col) throws Exception {
		if (this.getTable()[row][col] == '-') {
			table[row][col] = currentPlayer.getName();
		} else {
			throw new Exception();
		}
	}
        public boolean checkWinRow() {
		for (int row = 0; row < table.length; row++) {
			if (table[row][0] == table[row][1] && table[row][0] == table[row][2] && table[row][0] != '-') {
				return true;
			}
		}
		return false;
	}

	public boolean checkWinCol() {
		for (int col = 0; col < table.length; col++) {
			if (table[0][col] == table[1][col] && table[1][col] == table[0][col] && table[2][col] != '-') {
				return true;
			}
		}
		return false;
	}

	public boolean checkWinCross() {
		if (table[0][0] == table[1][1] && table[1][1] == table[2][2] && table[0][0] != '-') {
			return true;
		} else if (table[0][2] == table[1][1] && table[1][1] == table[2][0] && table[0][2] != '-') {
			return true;
		}
		return false;
	}

	public boolean checkWin() {
		if (checkWinRow() || checkWinCol() || checkWinCross()) {
			win = currentPlayer;
			if (currentPlayer == x) {
				x.Win();
				o.Lose();
			} else {
				x.Win();
				o.Lose();
			}
			return true;
		}
		return false;
	}

	public boolean checkDraw() {
		int count = 0;
		for (int i = 0; i < table.length; i++) {
			for (int j = 0; j < table.length; j++) {
				if (table[i][j] != '-') {
					count++;
				}
			}
		}
		if (count == 9 && checkWin() != true) {
			x.Draw();
			o.Draw();
			return true;
		}
		return false;
	}

	
}